import Koa from 'koa';
import Router from "koa-router";
import bodyparser from "koa-bodyparser";
import { addControllers } from './autoInjectController';

const app = new Koa();
const router = new Router();

app.use(bodyparser());

app.use(async (ctx, next) =>
{
    console.log(`Process ${ ctx.request.method } ${ ctx.request.url }...`);
    await next();
});

// await 
addControllers(router).then(() =>
{

    app.use(router.routes());

    app.listen(3000);

    console.log('app started at port 3000...');

});

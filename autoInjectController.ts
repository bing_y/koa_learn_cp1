import * as fs from 'fs';

function addMapping(router, mapping)
{
    for (var url in mapping)
    {
        if (url.startsWith('GET '))
        {
            var path = url.substring(4);
            router.get(path, mapping[url]);
            console.log(`register URL mapping: GET ${ path }`);
        } else if (url.startsWith('POST '))
        {
            var path = url.substring(5);
            router.post(path, mapping[url]);
            console.log(`register URL mapping: POST ${ path }`);
        } else
        {
            console.log(`invalid URL: ${ url }`);
        }
    }
}

export async function addControllers(router)
{
    var files = fs.readdirSync(__dirname + '/controllers');
    var js_files = files.filter((f) =>
    {
        return f.endsWith('.ts');
    });

    for (var f of js_files)
    {
        console.log(`process controller: ${ f }...`);
        const path = __dirname + '/controllers/' + f;
        // let mapping = require(path);
        // 
        const { routes } = await import(path);
        console.log(routes);
        addMapping(router, routes);
    }
}